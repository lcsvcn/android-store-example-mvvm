package com.lcsvcn.mvvmapp.repositories;

import android.arch.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;


/**
 * Singleton pattern
 */
public class NiceProductRepository {

    private static com.lcsvcn.mvvmapp.repositories.NiceProductRepository instance;
    private ArrayList<com.lcsvcn.mvvmapp.models.NiceProducts> dataSet = new ArrayList<>();

    public static com.lcsvcn.mvvmapp.repositories.NiceProductRepository getInstance(){
        if(instance == null){
            instance = new com.lcsvcn.mvvmapp.repositories.NiceProductRepository();
        }
        return instance;
    }


    // Pretend to get data from a webservice or online source
    public MutableLiveData<List<com.lcsvcn.mvvmapp.models.NiceProducts>> getNicePlaces(){
        setNicePlaces();
        MutableLiveData<List<com.lcsvcn.mvvmapp.models.NiceProducts>> data = new MutableLiveData<>();
        data.setValue(dataSet);
        return data;
    }

    private void setNicePlaces(){
        dataSet.add(
                new com.lcsvcn.mvvmapp.models.NiceProducts(
                        "https://qa-m1-dr.abi-sandbox.net/media/catalog/product/-/R/-R002151.png",
                        "Pepsi Lata",
                        1,
                        120,
                        10.20f,
                        10.60f
                        )
        );

        dataSet.add(
                new com.lcsvcn.mvvmapp.models.NiceProducts(
                        "https://qa-m1-dr.abi-sandbox.net/media/catalog/product/-/R/-R002151.png",
                        "Pepsi Light 24/12onz Lata",
                        24,
                        120,
                        603.90f,
                        603.90f
                )
        );

        dataSet.add(
                new com.lcsvcn.mvvmapp.models.NiceProducts(
                        "https://qa-m1-dr.abi-sandbox.net/media/catalog/product/-/R/-R002151.png",
                        "Seven Up Lata",
                        24,
                        120,
                        200.90f,
                        380.20f
                )
        );
    }
}











