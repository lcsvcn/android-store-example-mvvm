package com.lcsvcn.mvvmapp.models;

public class NiceProducts {

    private String title;
    private String imageUrl;
    private Integer quantity;
    private Integer volume;
    private Float oldPrice;
    private Float newPrice;
    private boolean hasPromotion;


    public NiceProducts(String imageUrl, String title, Integer quantity, Integer volume, Float newPrice, Float oldPrice) {
        this.title = title;
        this.imageUrl = imageUrl;
        this.quantity = quantity;
        this.volume = volume;
        this.oldPrice = oldPrice;
        this.newPrice = newPrice;
        hasPromotion = !oldPrice.equals(newPrice);
    }

    public NiceProducts() {
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
