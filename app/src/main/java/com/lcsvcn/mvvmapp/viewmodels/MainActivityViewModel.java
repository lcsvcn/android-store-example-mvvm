package com.lcsvcn.mvvmapp.viewmodels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;
import android.os.AsyncTask;

import java.util.List;

public class MainActivityViewModel extends ViewModel {

    private MutableLiveData<List<com.lcsvcn.mvvmapp.models.NiceProducts>> mNicePlaces;
    private com.lcsvcn.mvvmapp.repositories.NiceProductRepository mRepo;
    private MutableLiveData<Boolean> mIsUpdating = new MutableLiveData<>();

    public void init(){
        if(mNicePlaces != null){
            return;
        }
        mRepo = com.lcsvcn.mvvmapp.repositories.NiceProductRepository.getInstance();
        mNicePlaces = mRepo.getNicePlaces();
    }

    public void addNewValue(final com.lcsvcn.mvvmapp.models.NiceProducts niceProducts){
        mIsUpdating.setValue(true);

        new AsyncTask<Void, Void, Void>(){
            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                List<com.lcsvcn.mvvmapp.models.NiceProducts> currentPlaces = mNicePlaces.getValue();
                currentPlaces.add(niceProducts);
                mNicePlaces.postValue(currentPlaces);
                mIsUpdating.postValue(false);
            }

            @Override
            protected Void doInBackground(Void... voids) {

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                return null;
            }
        }.execute();
    }

    public LiveData<List<com.lcsvcn.mvvmapp.models.NiceProducts>> getNicePlaces(){
        return mNicePlaces;
    }


    public LiveData<Boolean> getIsUpdating(){
        return mIsUpdating;
    }
}
